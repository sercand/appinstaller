package app

import (
	"errors"
	"net"
	"strings"
)

func IpAdress(config *Config) (net.IP, error) {

	ifaces, err := net.Interfaces()

	if err != nil {
		return nil, err
	}
	for _, i := range ifaces {
		addrs, err := i.Addrs()
		if err != nil {
			return nil, err
		}
		for _, addr := range addrs {
			var ip net.IP
			switch v := addr.(type) {
			case *net.IPNet:
				ip = v.IP
			case *net.IPAddr:
				ip = v.IP
			}

			if strings.HasPrefix(ip.String(), config.IPPrefix) {
				return ip, nil
			}
		}
	}

	return nil, errors.New("Not found")
}
