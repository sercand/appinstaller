package app

type Device struct {
	ID      string `json:"id"`
	Type    string `json:"type"`
	Product string `json:"product"`
	OS      string `json:"os"`
	Name    string `json:"name,omitempty"`
}

const (
	Android = "android"
	IPad = "iPad"
	IPhone = "iPhone"
)

type Platform interface {
	Find() []Device
	Install(device string, packagePath string)
	Run(device string, activity string)
	GetInfo(device string, target string) string
	Uninstall(device string, packageName string)
}

var (
	adb = Adb{}
	ios = Apple{}
)

type devicesData struct {
	Android []Device `json:"android"`
	IOS     []Device `json:"ios"`
}

func DevicesEventHandler(c *Client, _ interface{}) {
	d := devicesData{}

	done := make(chan bool, 2)
	defer close(done)

	go func() {
		d.Android = adb.Find()
		done <- true
	}()
	go func() {
		d.IOS= ios.Find()
		done <- true
	}()

	for a := 1; a <= 2; a++ { <-done }
	c.Emit("devices", d)
}