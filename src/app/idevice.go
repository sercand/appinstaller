package app

import (
	"fmt"
	"os/exec"
	"runtime"
	"strings"
	"log"
	"path/filepath"
	"bytes"
	plist "github.com/DHowett/go-plist"
)

type iOSApp struct {
	Path             string `plist:"Path"`
	BundleIdentifier string `plist:"CFBundleIdentifier"`
}

func getAllInfo(device string) map[string]string {
	m := make(map[string]string)
	eol := "\n"
	if runtime.GOOS == "windows" {
		eol = "\r\n"
	}
	dataOut, err := exec.Command("ideviceinfo", "--udid", device).Output()
	if err != nil {
		log.Println("error:", err)
		return m
	}
	lines := strings.Split(string(dataOut), eol)
	for _, l := range lines {
		line := strings.Split(l, ":")
		if len(line) == 2 {
			m[line[0]] = strings.TrimSpace(line[1])
		}
	}
	return m
}

func stringInSlice(a string, list []string) bool {
	for _, b := range list {
		if b == a {
			return true
		}
	}
	return false
}

type Apple struct {
}

func (apple Apple) Find() []Device {
	data_out, err := exec.Command("idevice_id", "--list").Output()
	devices := make([]Device, 0)

	if err != nil {
		log.Println("error: ios find devices:", err)
		return devices
	}
	log.Println("error: ios find out:", string(data_out))

	eol := "\n"
	if runtime.GOOS == "windows" {
		eol = "\r\n"
	}

	strs := strings.Split(string(data_out), eol)
	device_ids := make([]string, 0)

	fmt.Println(device_ids)

	for _, d := range strs {

		if len(d) == 40 && !stringInSlice(d, device_ids) {
			device_ids = append(device_ids, d)
		}
	}

	jobs := make(chan *Device, 1)
	defer close(jobs)

	worker := func(id string, done chan *Device) {
		info := getAllInfo(id)
		_version := info["ProductVersion"]
		_product := info["ProductType"]
		_type := info["DeviceClass"]
		_name := info["DeviceName"]

		done <- &Device{
			Name:    _name,
			ID:      id,
			Product: _product,
			OS:      _version,
			Type:    _type,
		}
	}

	for _, d := range device_ids {
		go worker(d, jobs)
	}

	for a := 1; a <= len(device_ids); a++ {
		_device := <-jobs
		devices = append(devices, *_device)
	}

	return devices
}

func (apple Apple) GetInfo(device string, target string) string {
	info := getAllInfo(device)
	return info[target]
}

func (apple Apple) Install(device string, packagePath string) {
	data_out, err := exec.Command("ideviceinstaller", "--udid", device, "--install", packagePath).Output()
	if err != nil {
		log.Println("error: ios install", err)
		return
	}
	log.Println("info: install out:", string(data_out))
	SendNotify("Install", filepath.Base(packagePath) + " installed", IconInfo)
}

func (apple Apple) Uninstall(device string, packageName string) {
	data_out, err := exec.Command("ideviceinstaller", "--udid", device, "--uninstall", packageName).Output()
	if err != nil {
		log.Println("error: ios uninstall", err)
		return
	}
	log.Println("info: uninstall out:", string(data_out))
}

func (apple Apple) Run(device string, packageName string) {
	data_out, err := exec.Command("ideviceinstaller", "--udid", device, "-l", "-o", "xml").Output()
	if err != nil {
		log.Println("error: ios get applications", err)
		return
	}
	decoder := plist.NewDecoder(bytes.NewReader(data_out))

	apps := make([]*iOSApp, 0)

	err = decoder.Decode(&apps)
	if err != nil {
		log.Println("error: while decoding plist:", err)
	}
	for _, a := range apps {
		if a.BundleIdentifier == packageName {
			log.Println("info: App bundle and path: ", a.BundleIdentifier, a.Path)
		}
	}
}
