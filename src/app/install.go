package app

import (
	"io"
	"net/http"
	"os"
	"strings"
	"path"
	"encoding/json"
	"log"
	"time"
)

func downloadFile(url string) (string, error) {
	tokens := strings.Split(url, "/")
	fileName := path.Join(os.TempDir(), tokens[len(tokens) - 1])
	log.Println("Downloading", url, "to", fileName)

	output, err := os.Create(fileName)
	if err != nil {
		log.Println("error: while creating", fileName, "-", err)
		return fileName, err
	}
	defer output.Close()

	response, err := http.Get(url)
	if err != nil {
		log.Println("error: while downloading", url, "-", err)
		return fileName, err
	}
	defer response.Body.Close()

	n, err := io.Copy(output, response.Body)
	if err != nil {
		log.Println("error: ", n, "bytes and Error while downloading", url, "-", err)
		return fileName, err
	}

	return fileName, nil
}

func installOnDevice(device Device, data installData, filePath string, platform Platform, done chan bool) {
	log.Println("info:", device.Name, device.Product)

	if data.Uninstall && len(data.PackageName) > 0 {
		platform.Uninstall(device.ID, data.PackageName)
		log.Println("info:", "uninstalled on", device.Name)
	}

	platform.Install(device.ID, filePath)

	log.Println("info:", "installed on", device.Name)
	if data.Start && len(data.Activity) > 0 {
		platform.Run(device.ID, data.Activity)
	}

	done <- true
}

func installToDevices(data installData, filePath string, platform Platform) {
	devices := platform.Find()

	l := len(devices)
	if l == 0 {
		return
	}

	done := make(chan bool, l)
	defer close(done)

	for _, d := range devices {
		installOnDevice(d, data, filePath, platform, done)
	}

	for a := 1; a <= l; a++ { <-done }

	time.AfterFunc(time.Minute * 3, func() {
		err := os.Remove(filePath)
		if err == nil {
			log.Printf("info: %s is removed\n", filePath)
		}else {
			log.Printf("error: while removing file %s, %v", filePath, err)
		}
	})
}

func install(data installData) {
	log.Println(data)

	filePath, err := downloadFile(data.Url)
	if (err != nil) {
		log.Println("install error:", err)
		return
	}

	if path.Ext(filePath) == ".apk" {
		installToDevices(data, filePath, adb)
	}else if path.Ext(filePath) == ".ipa" {
		installToDevices(data, filePath, ios)
	}else {
		log.Println("error: Unknown extension only .ipa and .apk is supported")
	}
}

type installData struct {
	Url         string  `json:"url"`
	Start       bool    `json:"start"`
	Uninstall   bool    `json:"uninstall"`
	PackageName string  `json:"package"`
	Activity    string  `json:"activity"`
}

type respondOK struct {
	Success bool `json:"success"`
}

func InstallEventHandler(c *Client, data interface{}) {

	var d installData
	m, _ := json.Marshal(data)

	if err := json.Unmarshal(m, &d); err != nil {
		return
	}

	go install(d)
}