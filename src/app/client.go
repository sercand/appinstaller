package app

import (
	"golang.org/x/net/websocket"
	"log"
	"encoding/json"
	"time"
)

//Client is a websocket client
type Client struct {
	Events      map[string]*Event
	Socket      *websocket.Conn
	Config      *Config
	IsConnected bool
}

func (c *Client)find(eventName string) *Event {
	e := c.Events[eventName]
	if e != nil {
		return e
	}
	return c.add(eventName)
}

func (c *Client)add(eventname string) *Event {
	e := &Event{
		Name:eventname,
		Listeners: make([]EventHandler, 0),
	}
	c.Events[eventname] = e
	return e
}

func (c *Client)fire(event string, data interface{}) {
	ev := c.find(event)

	for _, h := range (ev.Listeners) {
		if h != nil {
			h(c, data)
		}
	}
}

//Emit sends data to master
func (c *Client) Emit(event string, data interface{}) {
	msg := &Message{
		Event:event,
		Data:data,
	}

	sent, err := json.Marshal(msg)
	if err!=nil {
		log.Println(err)
		return
	}
	if (c.IsConnected) {
		log.Println("sending", event)
		c.Socket.Write(sent)
	}
}

//On is calls when an event fired
func (c *Client) On(event string, callback EventHandler) {
	e := c.find(event)
	e.Listeners = append(e.Listeners, callback)
}

func (c *Client) Off(event string, callback func(interface{})) {
	//e := c.find(event)
	//todo
}

func (c *Client) Connect(count int) {
	if count > 0 {
		c.fire(ReconnectAttemptEvent, nil)
	}

	ws, err := websocket.Dial(c.Config.MasterWebSocketUrl(), "", c.Config.MasterOrigin())
	if (err!=nil) {
		log.Println(err)
		c.IsConnected = false

		if count==0 {
			c.fire(ErrorEvent, err)
		}else {
			c.fire(ReconnectFailedEvent, err)
		}
	}else {
		c.Socket = ws
		c.IsConnected = true
		if count==0 {
			c.fire(ConnectEvent, nil)
		}else {
			c.fire(ReconnectEvent, nil)
		}
	}
}

/*
func (c *Client)Disconnect() {
	err := c.Socket.Close()
	if err!=nil {
		log.Println(err)
	}else {
		log.Println("Connection closed")
	}
}
*/

func (c *Client)waitForMessage() {
	var err error
	for {

		var msg string

		//log.Println(msg)
		if err = websocket.Message.Receive(c.Socket, &msg); err != nil {
			break
		}
		var ms Message
		err = json.Unmarshal([]byte(msg), &ms)

		if err!=nil {
			log.Println("err parsing message:", err)
		}else {
			//fmt.Println("the message is:", ms)
			c.fire(ms.Event, ms.Data)
		}
	}
}

func (c *Client)Loop() {

	connectionCount := 0

	for {
		c.Connect(connectionCount)

		if (c.IsConnected) {
			connectionCount = 0;
			c.waitForMessage()
		}
		c.IsConnected = false
		c.fire(DisconnectEvent, nil)

		time.Sleep(time.Duration(connectionCount+1) * time.Second)

		connectionCount++
	}
}

//NewClient creates a new client
func NewClient(config *Config) *Client {
	log.Println("New client is creating")

	c := &Client{
		Events:make(map[string]*Event),
		Config:config,
		IsConnected:false,
	}

	return c
}
