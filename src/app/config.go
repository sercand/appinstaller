package app

type Config struct {
	UserName         string
	MasterServerHost string
	IPPrefix         string
	ShowMessages     bool
	Version          string
}

func NewConfig() *Config {
	config := Config{
		UserName:        "user",
		IPPrefix:        "192.168.",
		MasterServerHost: "192.168.168.200:5555",
		ShowMessages:    true,
	}
	return &config
}

func (c *Config)MasterOrigin() string {
	return "http://" + c.MasterServerHost
}

func (c *Config)MasterWebSocketUrl() string {
	return "ws://" + c.MasterServerHost
}