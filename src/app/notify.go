package app

import (
	"os/exec"
	"runtime"
)

const (
	IconInfo string = "info"
	IconImportant string = "important"
	IconError string = "error"
)

func SendNotify(title, message, icon string) {
	if runtime.GOOS == "windows" {
		exec.Command("notify-send.exe", "-i", icon, title, message).Start()
	}
}