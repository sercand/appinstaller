package app


const (
//ConnectEvent Fired upon connecting.
	ConnectEvent string = "connect"
//ReconnectEvent Fired upon a connection error
	ReconnectEvent        string = "reconnect"
	DisconnectEvent       string = "disconnect"
	MessageEvent          string = "message"
	ErrorEvent            string = "error"
	ReconnectAttemptEvent string = "reconnect_attempt"
	ReconnectFailedEvent  string = "reconnect_failed"
)

type Message struct {
	Event string        `json:"event"`
	Data  interface{}    `json:"data"`
}

type EventHandler func(*Client, interface{})

type Event struct {
	Name      string
	Listeners [] EventHandler
}