package app

import (
	_ "fmt"
	"os/exec"
	"fmt"
	"strings"
	"path/filepath"
)

func shellCommand(device, command string) string {
	dateCmd := exec.Command("adb", "-s", device, "shell", command)
	dateOut, err := dateCmd.Output()
	if err != nil {
		fmt.Println("error:", err, dateOut)
		return ""
	}
	return string(dateOut)
}

type Adb struct {
}

func (adb Adb) Find() []Device {
	dateCmd := exec.Command("adb", "devices")
	devices := make([]Device, 0)

	data_out, err := dateCmd.Output()
	if err != nil {
		println("error:", err)
		return devices
	}
	strs := strings.Split(string(data_out), "\n")
	device_ids := make([]string, 0)

	for _, d := range strs {
		o := strings.Split(d, "\t")
		if (len(o) == 2 && (o[1] == "device" || o[1] == "device\r") && !stringInSlice(o[0], device_ids) ) {
			device_ids = append(device_ids, o[0])
		}
	}

	jobs := make(chan *Device, 1)
	defer close(jobs)

	worker := func(id string, done chan *Device) {
		_version := strings.TrimSuffix(adb.GetInfo(id, "ro.build.version.release"), "\r\r\n")
		_product := strings.TrimSuffix(adb.GetInfo(id, "ro.build.product"), "\r\r\n")
		done <- &Device{
			ID:      id,
			Product: _product,
			OS:      _version,
			Type:    Android,
			Name:    _product,
		}
	}

	for _, d := range device_ids {
		go worker(d, jobs)
	}

	for a := 1; a <= len(device_ids); a++ {
		_device := <-jobs
		devices = append(devices, *_device)
	}
	return devices
}

func (adb Adb) Install(deviceId string, packagePath string) {
	cmd := exec.Command("adb", "-s", deviceId, "install", "-r", "-d", packagePath)
	data_out, err := cmd.Output()
	if err != nil {
		fmt.Println("error:", err)
		return
	}
	fmt.Println(string(data_out))
	SendNotify("Install", filepath.Base(packagePath)+" installed", IconInfo)
}

func (adb Adb) Run(deviceId string, activity string) {
	cmd := exec.Command("adb", "-s", deviceId, "shell", "am", "start",
		"-S", "-a", "android.intent.action.MAIN", "-n", activity)
	data_out, err := cmd.Output()
	if err != nil {
		fmt.Println("error:", err)
		return
	}
	fmt.Println(string(data_out))
}

func (adb Adb) GetInfo(deviceId string, target string) string {
	return shellCommand(deviceId, "getprop "+target)
}

func (adb Adb) Uninstall(deviceId string, packageName string) {
	shellCommand(deviceId, "pm uninstall "+packageName)
}

