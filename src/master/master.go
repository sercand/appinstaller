package main

import(
	"fmt"
	"net/http"
	"io/ioutil"
	"log"
	"os"
)

func handler(w http.ResponseWriter, r *http.Request) {
	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		fmt.Println(w, "OK")
		return
	}
	log.Println(string(body))
	fmt.Println(w, "OK")
}

func main() {
	f, err := os.OpenFile("testlogfile", os.O_RDWR | os.O_CREATE | os.O_APPEND, 0666)
	if err != nil {
		fmt.Println("error opening file: %v", err)
		return
	}
	defer f.Close()

	log.SetOutput(f)
	http.HandleFunc("/", handler)
	http.ListenAndServe(":8080", nil)
}
