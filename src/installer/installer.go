package main

import (
	"app"
	"encoding/json"
	"flag"
	"log"
	"os"
	"path/filepath"
	_ "strconv"
	"time"
)

const version = "0.3.0"

type DeviceInfo struct {
	User    string `json:"user"`
	Version string `json:"version"`
}
type devicesData struct {
	Android []app.Device `json:"android"`
	IOS     []app.Device `json:"ios"`
}

func removeOldPackages() {
	filepath.Walk(os.TempDir(), func(p string, f os.FileInfo, err error) error {
		if filepath.Ext(p) == ".ipa" || filepath.Ext(p) == ".apk" {
			err := os.Remove(p)
			if err == nil {
				log.Printf("info: %s is removed\n", p)
			} else {
				log.Printf("error: while removing file %s, %v", p, err)
			}
		}
		return nil
	})
}

func main() {
	log.Println("Installer is starting!!!")

	config := app.NewConfig()
	config.Version = version

	flag.StringVar(&config.UserName, "user", "<user>", "User name")
	flag.StringVar(&config.IPPrefix, "prefix", config.IPPrefix, "IP Prefix, app looking for")
	flag.BoolVar(&config.ShowMessages, "messages", config.ShowMessages, "Show notifications")
	flag.StringVar(&config.MasterServerHost, "url", config.MasterServerHost, "The url of build device")

	flag.Parse()

	ticker := time.NewTicker(10 * time.Minute)
	quit := make(chan struct{})
	go func() {
		for {
			select {
			case <-ticker.C:
				removeOldPackages()
			case <-quit:
				ticker.Stop()
				return
			}
		}
	}()
	removeOldPackages()

	if config.UserName == "<user>" {
		flag.Usage()
		log.Fatalln("You must enter user flag!!!")
	}

	output, _ := json.Marshal(config)
	log.Println("Server is starting with following conf", string(output))

	client := app.NewClient(config)

	client.On(app.ConnectEvent, func(c *app.Client, _ interface{}) {
		log.Println("ConnectEvent")
		//c.Emit("info", DeviceInfo{
		//	User:config.UserName,
		//	Version:version,
		//})
	})

	client.On(app.ErrorEvent, func(client *app.Client, _ interface{}) {
		log.Println("ErrorEvent")
	})

	client.On(app.ReconnectEvent, func(client *app.Client, _ interface{}) {
		log.Println("ReconnectEvent")
	})

	client.On(app.ReconnectAttemptEvent, func(client *app.Client, _ interface{}) {
		log.Println("ReconnectAttemptEvent")
	})

	client.On(app.ReconnectFailedEvent, func(client *app.Client, _ interface{}) {
		log.Println("ReconnectFailedEvent")
	})

	client.On("info", func(c *app.Client, data interface{}) {
		c.Emit("info", DeviceInfo{
			User:    config.UserName,
			Version: version,
		})
	})

	client.On("devices", app.DevicesEventHandler)
	client.On("install", app.InstallEventHandler)

	client.Loop()
}
